# -*- coding: utf-8 -*-
{
    'name': "OneSignal Notification",

    'summary': """
        Send push notification using one-signal""",

    'description': """
        Send push notification using one-signal
    """,

    'author': "Mazharul Sabbir",
    'website': "https://www.mazharulsabbir.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/14.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Tools',
    'version': '15.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'contacts'],

    # always loaded
    'data': [
        'security/onesignal_security.xml',
        'security/ir.model.access.csv',
        'data/sequence.xml',

        'views/notification.xml',
        'views/res_company_inherit.xml',
        'views/menus.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': False,
    'icon': "/onesignal_notification/static/description/icon.png",
    'images': ['static/description/icon.png'],
    "license": "OPL-1",
    "price": 0,
    "currency": "EUR",
}
