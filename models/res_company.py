# -*- coding: utf-8 -*-

import string
from odoo import models, fields, api
from requests.exceptions import HTTPError


class ResCompanyInherit(models.Model):
    _inherit = "res.company"

    app_id = fields.Char(string="App id")
    rest_api_key = fields.Char(string="RestAPI key")
    external_partner_prefix = fields.Char(string="External Partner Prefix", default="partner_", help="Enter your external user id prefix. It will be used to create external user id for each partner. For example, if you enter 'partner_' then external user id for partner with id 1 will be 'partner_1'")
