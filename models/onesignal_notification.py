# -*- coding: utf-8 -*-

import logging
from odoo import models, fields, api
from requests.exceptions import HTTPError

import requests
import json

_logger = logging.getLogger(__name__)


class OneSignalNotification(models.Model):
    _name = 'onesignal.notification'
    _description = 'Send push notification using onesignal'
    _order = "id desc"

    # this fields will be auto generated
    name = fields.Char(string="Code", default=lambda self: self.env['ir.sequence'].next_by_code('onesignal.notification.sequence'))
    os_response = fields.Char(string="Onesignal Response")
    company_id = fields.Many2one(comodel_name="res.company", index=True, string="Company", default=lambda self: self.env.company)

    # these fields are required to create notification
    title = fields.Char(string="Title", default=lambda self: self.env.company.name)
    body = fields.Text(string="Message")
    status = fields.Selection(
        [('draft', 'Draft'), ('sent', 'Sent'), ('failed', 'Failed')],
        default="draft",
        string="Status"
    )
    include_partner_ids = fields.Many2many(
        comodel_name="res.partner",
        relation="notification_partner_rel",
        column1="os_notification_id",
        column2="partner_id",
        string="Included Users",
        store=True,
    )

    # optional fields to track later
    external_user_id = fields.Char('Notification Id')
    rec_model = fields.Char('Model Name')
    rec_id = fields.Integer('Record Id')

    @api.model
    def create(self, vals):
        record = super(OneSignalNotification, self).create(vals)

        try:
            external_user_ids = []
            for external_id in record.include_partner_ids.ids:
                onesignal_external_user_id = external_id
                if record.company_id.external_partner_prefix:
                    onesignal_external_user_id = str(record.company_id.external_partner_prefix) + str(onesignal_external_user_id)

                external_user_ids.append(onesignal_external_user_id)

            if record.external_user_id:
                external_user_ids.append(record.external_user_id)

            payload = json.dumps({
                "app_id": record.company_id.app_id,
                "include_external_user_ids": external_user_ids,
                "headings": {
                    "en": record.title
                },
                "contents": {
                    "en": record.body
                },
                "data": {
                    "custom_data1": "if any custom data.",
                    "custom_data2": "if any custom data."
                }
            })

            headers = {
                'Authorization': f'Basic {record.company_id.rest_api_key}',
                'Content-Type': 'application/json'
            }

            # Sends it!
            result = requests.request("POST", "https://onesignal.com/api/v1/notifications", headers=headers, data=payload)

            try:
                print(result.text)
                response_json = result.json()
                res_msg = "sent" if response_json.get("recipients", 0) > 0 else "failed"
                record.write({
                    'status': res_msg,
                    "os_response": str(result.json())
                })
            except Exception as e:
                res_msg = str(e)
                record.os_response = str(res_msg)
            _logger.info("*** Push Notification Sent ***")

        except HTTPError as e:
            result = e.response.json()
            record.write({
                'status': "failed",
                "os_response": str(result)
            })
            _logger.error("*** Push Notification Failed! ***")
            _logger.error(str(e))

        return record
