# -*- coding: utf-8 -*-
import json

from odoo import http
from odoo.http import request, Response


class Notification(http.Controller):
    @http.route('/api/v1/notifications', auth='public', type='http', methods=['GET', 'OPTIONS'], cors='*', csrf=False)
    def notification_list(self, **kw):
        page_number = int(kw.get('page_number', 1))
        per_page = int(kw.get('per_page', 10))

        offset = 0 if page_number <= 1 else (page_number - 1) * per_page

        domain = [('include_partner_ids', 'in', [request.env.user.partner_id.id]), ('status', '=', 'sent')]
        notification_ids = request.env['onesignal.notification'].sudo().search(domain, offset=offset, limit=per_page)

        response = [{
            'id': notification_id.id,
            'title': notification_id.title or None,
            'message': notification_id.body or None,
            'state': notification_id.status.capitalize()
        } for notification_id in notification_ids]

        result = {
            'success': True,
            'data': response,
            'page_number': page_number if page_number > 0 else 1,  # current page number
            'last_page': len(response) == 0 or len(response) < per_page,
            'per_page': per_page,  # records per page
        }
        return Response(
            json.dumps(result, sort_keys=False, indent=4),
            content_type='application/json;charset=utf-8',
            status=200
        )
